#ifdef __CINT__
#pragma link C++ class vector<Double_t>+;
#pragma link C++ class cHit+;
#pragma link C++ class cRawEvent+;
#pragma link C++ class cLookupTable+;
#pragma link C++ class cRawSignal+;
#pragma link C++ class cPhysicalHit+;
#pragma link C++ class cPhysicalEvent+;
#endif

#ifdef _MAKECINT_
//#pragma link C++ class vector<double>+;
#pragma link C++ class vector<cHit>+;
#pragma link C++ class map<Int_t, cLookupTable::chanData>+;
#pragma link C++ class vector<cRawSignal>+;
#pragma link C++ class list<cPhysicalHit>+;
#endif
